Dvorak-Qwerty for Linux XKB configuration files
------
These keymaps using first level to redirect to different keycode in inet "evdev" section that won't used in ordinary keyboard. Any keys work as Qwerty when press with Control, Alt or Super. 2-5 levels works as same as 1-4 levels of original layout. created and tested on Arch Linux 2021.07.01.

Usage
------
1.	Backup your data before any changes
2.	Clone git repository (from one of the three links below):
	-	git clone https://gitlab.com/rabo86/dvorak-qwerty.git
3. 	Make the file executable by running:
	-	sudo chmod +x ./dvorak-qwerty/dvorak-qwerty.sh
4.	Run the script:
	-	./dvorak-qwerty/dvorak-qwerty.sh
5. After reboot or Alt+F2+r+Enter, you can choose new input sources from the Setting->Keyboard->Input Sources:
	-	Dvorak-Qwerty
	-	Dvorak-Qwerty simplified (no dead keys)
	-	Dvorak-Qwerty, international with dead keys
	-	Dvorak-Qwerty alternative international no dead keys
	-	Dvorak-Qwerty, classic
	-	Dvorak-Qwerty, programmer
6. You may need reassigning shortcuts in system setting.
