#!/bin/bash

# Adding Dvorak-Qwerty layout
sudo cp /usr/share/X11/xkb/rules/base.lst /usr/share/X11/xkb/rules/base.lst.bak
sudo cp /usr/share/X11/xkb/rules/base.xml /usr/share/X11/xkb/rules/base.xml.bak
sudo cp /usr/share/X11/xkb/rules/evdev.lst /usr/share/X11/xkb/rules/evdev.lst.bak
sudo cp /usr/share/X11/xkb/rules/evdev.xml /usr/share/X11/xkb/rules/evdev.xml.bak
sudo cp /usr/share/X11/xkb/symbols/inet /usr/share/X11/xkb/symbols/inet.bak
sudo cp /usr/share/X11/xkb/symbols/us /usr/share/X11/xkb/symbols/us.bak
sudo cp /usr/share/X11/xkb/types/complete /usr/share/X11/xkb/types/complete.bak
CONF=/usr/share/X11/xkb
sudo cp -R ./dvorak-qwerty/rules ./dvorak-qwerty/symbols ./dvorak-qwerty/types $CONF
sudo rm -rf ./dvorak-qwerty
